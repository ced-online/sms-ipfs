var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var Web3 = require('web3');

var fileUpload = require("express-fileupload");

var MyContractJSON =require(path.join(__dirname, 'build/contracts/StudentManagementSystem.json'));
web3 = new Web3("http://localhost:7545");

//-- Edit Start Here ---
coinbase = "0x3f1211aa2E9bafd918c532303Ad756d924192E5B";
contractAddress = MyContractJSON.networks['5777'].address;
var contractAbi = MyContractJSON.abi;
//-- Edit Ends Here  ---

SMS = new web3.eth.Contract(contractAbi, contractAddress);
console.log(SMS);
var indexRouter = require('./routes/index');
var setStudentRouter = require('./routes/setStudent');
var getStudentRouter = require('./routes/getStudent');
var feePaymentRouter = require('./routes/feePayment');

var uploadCertificateRouter = require("./routes/uploadCertificate");
var viewCertificateRouter = require("./routes/viewCertificate");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(fileUpload());

app.use('/', indexRouter);
app.use('/setStudent', setStudentRouter);
app.use('/getStudent', getStudentRouter);
app.use('/feePayment', feePaymentRouter);

app.use("/uploadCertificate", uploadCertificateRouter);
app.use("/viewCertificate", viewCertificateRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
